var express = require("express");
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var passport = require('passport');
var saml = require('passport-saml');
var fs = require('fs');

// initialize express app
var app = express();

// set cookie and session defaults to app
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session({secret: 'my-precious-secret', 
                 resave: false, 
                 saveUninitialized: true,}));

// saml operations here
passport.serializeUser(function(user, done) {
    console.log('-----------------------------');
    console.log('serialize user');
    console.log(user);
    console.log('-----------------------------');
    done(null, user);
});
passport.deserializeUser(function(user, done) {
    console.log('-----------------------------');
    console.log('deserialize user');
    console.log(user);
    console.log('-----------------------------');
    done(null, user);
});

var samlStrategy = new saml.Strategy({
        // config options here
        callbackUrl: 'http://192.168.1.77:4300/login/callback', // 'http://localhost/login/callback',
        entryPoint: 'http://192.168.1.77:8080/simplesaml/saml2/idp/SSOService.php', // 'http://localhost:8080/simplesaml/saml2/idp/SSOService.php',
        issuer: 'saml-poc',
        identifierFormat: null,
        decryptionPvk: fs.readFileSync(__dirname + '/certs/key.pem', 'utf8'),
        privateCert: fs.readFileSync(__dirname + '/certs/key.pem', 'utf8'),
        validateInResponseTo: false,
        disableRequestedAuthnContext: true,
        cert: fs.readFileSync(__dirname + '/certs/idp_key.pem', 'utf8'), // added by me :)
    },
    function(profile, done) {
        return done(null, profile);
    }
    );

passport.use('samlStrategy', samlStrategy);

app.use(passport.initialize({}));
app.use(passport.session({}));

// root route defined here
app.get('/',
        function(req, res) {
            res.send('Test Home Page');
        }
        );

// define login route
app.get('/login',
    function (req, res, next) {
        console.log('-----------------------------');
        console.log('/Start login handler');
        next();
    },
    passport.authenticate('samlStrategy'),
);

// login callback (from Idp) handler route
app.post('/login/callback',
    function (req, res, next) {
        console.log('-----------------------------');
        console.log('/Start login callback ');
        next();
    },
    passport.authenticate('samlStrategy'),
    function (req, res) {
        console.log('-----------------------------');
        console.log('login call back dumps');
        console.log(req.user);
        console.log('-----------------------------');
        res.send('Log in Callback Success');
    }
);

// start the app
var server = app.listen(4300, function () {
    console.log('Listening on port %d', server.address().port);
});